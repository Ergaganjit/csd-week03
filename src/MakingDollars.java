import java.util.Scanner;

public class MakingDollars {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*You have an amazing job!   Your job pays you a salary that doubles every day
$1 on day 1
$2 on day 2
$4 on day 3
$8 on day 4
$16 on day 5
etc
Write a program that:
Asks the user for how many days they will work
Outputs how much $$ they make EACH day
Outputs the total amount earned at the end of the time period
*/
		
		//1.User Input 
		Scanner keyboard=new Scanner(System.in);
		System.out.println("How many days you working  ?");
		int days  = keyboard.nextInt();
		double total=0;
		//2. Calculate some dollars 
		for(int i =1; i<=days;i++ )
		{ 
			
			// day1 -1 ---2 raise to power 0 
			// day 2 -2 ----2 raise to power 1 means 2^(days-1)
			
			double salaryToday=Math.pow(2, i-1);
			System.out.println("Money on day"+i+": $"+salaryToday);
			
			//How much money you made in total ? 
			
			total=total+salaryToday;
			System.out.println("Total so far"+total);
		}
		
		//3. Output 
		System.out.println("-----------------------------------");
		System.out.println("Total Dollars : $ "+total);
		
		

	}

}
