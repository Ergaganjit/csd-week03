import java.util.Scanner;

public class SoftwareSales {

	public static void main(String[] args) {
		// 1. Ask the user for input 
		// -How many softwares you want ?
		Scanner keyboard=new Scanner(System.in);
		System.out.println("How many softwares you want?");
		int numSoftware= keyboard.nextInt();
		
		
		double discount=0;
		// 2.Calculate the discount 
		
		if(numSoftware>=10 && numSoftware<19)
		{
			//discount =20%
			discount=0.02;
		}
		else if (numSoftware>=20 && numSoftware <49)
		{
			//discount =30%
			discount=0.03;
		
		}
		else if(numSoftware>=50 && numSoftware<99)
		{
			//discount =40%
			discount=0.04;
		}
		else if (numSoftware>=100)
		{
			//discount =50%
			discount=0.05;
		}
		
		double subtotal= 99*numSoftware;
		double discountAmount =subtotal*discount ;
		double finalAmount = subtotal-discountAmount;
		
		
		// 3. show the output 
		System.out.println("Subtotal is: $"+subtotal);
		System.out.println("Discount Percent: "+discount*100+"%");
		System.out.println("Discount is: $"+discount);
		System.out.println("Final Amount is: $"+finalAmount);
		
		
		//Rounded to 2 decimal place WE USE printf() not Println
		
		System.out.printf("Final Price: %.3f   gaganjit kaur ", finalAmount); // %.2f symbol and  everything that comes after % sign is the placeholder 
		System.out.printf("apple %d banana\n ", numSoftware); //%d is for integers not for double 
		System.out.printf("apple %f banana\n ", finalAmount); //default is 4 decimal points 
		System.out.printf("apple %.1f banana\n ", finalAmount); //%f for decimal and %.1f means count till 1 decimal point 
		//String Formatting 
		
		
		// --> subtotal 
		// -->discount amount 
		// --> Total 
		
		

	}

}
